#!/usr/bin/env python3

from flask import Flask, render_template, request
app = Flask(__name__)

from datetime import datetime

@app.route("/")
def home():
    return render_template('index.html')

@app.route("/greetings", methods=["POST"])
def greet():
    return render_template('greet.html', name=request.form["name"], date=datetime.now())

if __name__ == "__main__":
    app.run()