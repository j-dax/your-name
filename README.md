# simple continuous integration and continuous deployment

This project incorporates a simple Flask application, integrated
and deployed using both GitLab and Heroku.

## Project requirements

[Python3](https://www.python.org/downloads/)

if you'd like to run the project locally,
```
python -m pip install -r requirements.txt
flask run
```

## Deployed

[Heroku dyno](https://your-name-app-production.herokuapp.com/)
